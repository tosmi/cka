#!/bin/bash

set -euf -o pipefail

export PATH=/bin:/sbin:/usr/bin:/usr/sbin

mkdir -p "$HOME"/.kube
cp -i /etc/kubernetes/admin.conf "$HOME"/.kube/config
chown $(id -u):$(id -g) "$HOME"/.kube/config

mkdir /home/vagrant/.kube
cp -i /etc/kubernetes/admin.conf /home/vagrant/.kube/config
chown -R vagrant:vagrant /home/vagrant/.kube/

# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  config.vm.provision "shell", inline: <<-SHELL
      apt-get update
      apt-get upgrade -y
      apt-get install -y docker.io
      cat<<EOF > /etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF
      curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
      apt-get update
      apt-get install --allow-downgrades -y kubeadm=1.11.4-00 kubectl=1.11.4-00 kubelet=1.11.4-00 jq
      apt-mark hold kubeadm kubectl kubelet
      echo 'source /vagrant/bash_config' >> /home/vagrant/.bashrc
      echo 'source /vagrant/bash_config' >> /root/.bashrc
      echo '172.17.1.2 master master.local' >> /etc/hosts
      echo '172.17.1.3 node node.local' >> /etc/hosts
      usermode -aG systemd-journal vagrant
  SHELL

  config.vm.define "master", primary: true do |master|
    master.vm.box = "ubuntu/xenial64"
    master.vm.hostname = "master"
    master.vm.box_check_update = false
    master.vm.network "private_network", ip: "172.17.1.2"
    # config.vm.network "forwarded_port", guest: 80, host: 8080
    master.vm.provider "virtualbox" do |vb|
      vb.memory = "2048"
      vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      vb.name ="k8s_master"
    end

    master.vm.provision "shell", inline: <<-SHELL
      echo 'master' > /etc/hostname
      sed -ie 's/ubuntu-xenial/master/g' /etc/hosts
    SHELL
  end

  config.vm.define "node" do |node|
    node.vm.box = "ubuntu/xenial64"
    node.vm.hostname = "node"
    node.vm.box_check_update = false
    node.vm.network "private_network", ip: "172.17.1.3"
    node.vm.provider "virtualbox" do |vb|
      vb.memory = "2048"
      vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      vb.name ="k8s_node"
    end

    node.vm.provision "shell", inline: <<-SHELL
      echo 'node' > /etc/hostname
      sed -ie 's/ubuntu-xenial/node/g' /etc/hosts
    SHELL
  end
end
